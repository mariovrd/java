/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario.java_vjezba6;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author Mario
 */
public class WaterFlowMeter {
    @JsonProperty
    private String broker;
    @JsonProperty
    private ArrayList<Sensor> sensors;
    @JsonIgnore
    private ArrayList<Thread> threads;

    public WaterFlowMeter() {
        this.sensors = new ArrayList<> ();
        this.threads = new ArrayList<> ();
        this.broker = "tcp://192.168.0.14:1883";
        this.sensors.add(new Sensor("Trenutna temperatura", 10, -32668, 32668, "C"));
        this.sensors.add(new Sensor("Trenutni tlak", 1000, 0, 65336, "Bar"));
        this.sensors.add(new Sensor("Potrosnja u zadnjih 1min, 10min, 1h i 1 dan", 0, 0, 65336, "litra"));
        this.sensors.add(new Sensor("Potrosnja u zadnjih 1 tjedan, 1 mjesec, 1 godinu", 10, 0, 65336, "litra"));
    }
    
    @JsonCreator
    public WaterFlowMeter(@JsonProperty("name") String name, @JsonProperty("factor") int factor, 
            @JsonProperty("min") int min, @JsonProperty("max") int max, 
            @JsonProperty("unit") String unit) {
        this.sensors = new ArrayList<> ();
        this.sensors.add(new Sensor(name, factor, min, max, unit));
    }

    public void startPublishing() throws MqttException {
        int qos = 2;
        String clientId = "Mjerac";
        MemoryPersistence persistence = new MemoryPersistence();
        MqttClient publisher = new MqttClient(this.broker, clientId, persistence);
        try {
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: " + this.broker);
            publisher.connect(connOpts);
            System.out.println("Connected");
            for(Sensor sensor : sensors) {
                sensor.setPublisherClient(publisher);
                new Thread(sensor).start();
            }
        } catch (MqttException me) {
            System.out.println("msg " + me.getMessage());
            System.out.println("excep " + me);
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario.java_vjezba6;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author Mario
 */
public class Sensor implements Runnable {
    @JsonProperty
    private final String name;
    private float value;
    @JsonProperty
    private final int factor;
    @JsonProperty
    private final int min;
    @JsonProperty
    private final int max;
    @JsonProperty
    private final String unit;
    
    @JsonIgnore
    private MqttClient publisherClient;
    
    
    @JsonCreator
    public Sensor(@JsonProperty("name") String name, @JsonProperty("factor") int factor, 
            @JsonProperty("min") int min, @JsonProperty("max") int max, 
            @JsonProperty("unit") String unit) {
        this.name = name;
        this.factor = factor;
        this.min = min;
        this.max = max;
        if (this.factor != 0) {
            this.value = (ThreadLocalRandom.current()
                    .nextInt(this.min, this.max + 1)) / (float)this.factor;
        } else {
            this.value = ThreadLocalRandom.current().nextInt(this.min, this.max + 1);
        }
        this.unit = unit;
    }
    
    public void updateValue() {
        if (this.factor != 0) {
            this.value = (ThreadLocalRandom.current()
                    .nextInt(this.min, this.max + 1)) / (float)this.factor;
        } else {
            this.value = ThreadLocalRandom.current().nextInt(this.min, this.max + 1);
        }
    }
    
    @JsonIgnore
    public String getData() {
        return this.name + "\nFaktor: " + this.factor + 
                "\nRaspon: " + this.min + " do " + this.max + 
                "\nVrijednost: " + this.value + "\nJedinica: " + this.unit + "\n";
    }
    
    public String getName() {
        return this.name;
    }
    
    public MqttClient getPublisherClient() {
        return publisherClient;
    }

    public void setPublisherClient(MqttClient publisherClient) {
        this.publisherClient = publisherClient;
    }

    
    public void publish() {

    }

    @Override
    public void run() {
        while (true) {
            String topic = "mjerac/" + this.getName();
            String content = this.getData();
            System.out.println("Publishing message: " + content);
            MqttMessage message = new MqttMessage(content.getBytes());
            //message.setQos(2);
            try {
                publisherClient.publish(topic, message);
                System.out.println("Message published");
            } catch (MqttException ex) {
                Logger.getLogger(Sensor.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                TimeUnit.SECONDS.sleep(5);
            }   
            catch (InterruptedException ex) {
                Logger.getLogger(Sensor.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.updateValue();
            System.out.println("Updated value in thread : " + Thread.currentThread().getName());
        }
    }
}

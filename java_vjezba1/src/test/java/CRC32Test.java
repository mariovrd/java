/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mariovrdoljak.CRC32;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mario
 */
public class CRC32Test {
    
    public CRC32Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getValue method, of class CRC32.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        CRC32 instance = new CRC32();
        long expResult = 0L;
        long result = instance.getValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of reset method, of class CRC32.
     */
    @Test
    public void testReset() {
        System.out.println("reset");
        CRC32 instance = new CRC32();
        instance.reset();
        long expResult = 0L;
        long result = instance.getValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of update method, of class CRC32.
     */
    @Test
    public void testUpdate_int() {
        System.out.println("update");
        int b = 56;
        java.util.zip.CRC32 expected = new java.util.zip.CRC32();
        CRC32 result = new CRC32();
        expected.update(b);
        result.update(b);
        long expResult = expected.getValue();
        long myResult = result.getValue();
        assertEquals(expResult, myResult);
    }

    /**
     * Test of update method, of class CRC32.
     */
    @Test
    public void testUpdate_3args() {
        System.out.println("update");
        byte[] bytes = {2, 5, 32, 67};
        int offset = 0;
        int length = bytes.length;
        java.util.zip.CRC32 expected = new java.util.zip.CRC32();
        CRC32 result = new CRC32();
        expected.update(bytes, offset, length);
        result.update(bytes, offset, length);
        long expResult = expected.getValue();
        long myResult = result.getValue();
        assertEquals(expResult, myResult);
    }

    /**
     * Test of update method, of class CRC32.
     */
    @Test
    public void testUpdate_byteArr() {
        System.out.println("update");
        byte[] b = {54, 43, 7, 22};
        java.util.zip.CRC32 expected = new java.util.zip.CRC32();
        CRC32 result = new CRC32();
        expected.update(b);
        result.update(b);
        long expResult = expected.getValue();
        long myResult = result.getValue();
        assertEquals(expResult, myResult);
    }
    
}

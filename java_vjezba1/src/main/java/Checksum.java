/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mario
 */
public interface Checksum {
    void update(int b);
    void update(byte[] bytes, int offset, int length);
    long getValue();
    void reset();
}

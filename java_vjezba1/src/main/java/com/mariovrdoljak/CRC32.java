package com.mariovrdoljak;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mario
 */
public class CRC32 implements Checksum {
    private int crc;
    private final long poly;
    private final int[] crcTable;
    
    public CRC32() {
        this.crc = 0;
        this.poly = 0xedb88320;
        this.crcTable = createCRCTable();
        
    }

    private int[] createCRCTable() {
        int[] table = new int[256];
        for (int i = 0; i < 256; i++) {
            int r = i;
            for (int j = 7; j >= 0; j--) {
                if ((r & 1) != 0) {
                    r = (int) (poly ^ (r >>> 1));
                } else {
                    r = r >>> 1;
                }
            }
            table[i] = r;
        }
        return table;
    }

    @Override
    public long getValue() {
        return (long) crc & 0xffffffffL;
    }

    @Override
    public void reset() {
        crc = 0;
    }

    /**
     * 
     * Updates the checksum with the int b.
     * The byte is taken as the lower 8 bits of b
     * 
     */
    @Override
    public void update(int b) {
        int c = ~crc;
        c = crcTable[(c ^ b) & 0xff] ^ (c >>> 8);
        crc = ~c;
    }

    /**
     * 
     * Adds the byte array to the data checksum. 
     * 
     * @param bytes
     * the buffer which contains the data 
     * @param offset the offset in the
     * buffer where the data starts 
     * @param length the length of the data
     * 
     */
    @Override
    public void update(byte[] bytes, int offset, int length) {
        if (bytes == null) {
            return;
        }
        int c = ~crc;
        while (length > 0) {
            c = crcTable[(c ^ bytes[offset++]) & 0xff] ^ (c >>> 8);
            length--;
        }
        crc = ~c;
    }

    /**
     * 
     * Adds the complete byte array to the data checksum. 
     * 
     * @param b
     */
    public void update(byte[] b) {
        if (b == null) {
            return;
        }
        update(b, 0, b.length);
    }
}

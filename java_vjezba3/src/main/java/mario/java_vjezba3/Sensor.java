/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario.java_vjezba3;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Mario
 */
public class Sensor {
    @JsonProperty
    private final String name;
    private float value;
    @JsonProperty
    private final int factor;
    @JsonProperty
    private final int min;
    @JsonProperty
    private final int max;
    @JsonProperty
    private final String unit;
    
    
    @JsonCreator
    public Sensor(@JsonProperty("name") String name, @JsonProperty("factor") int factor, 
            @JsonProperty("min") int min, @JsonProperty("max") int max, 
            @JsonProperty("unit") String unit) {
        this.name = name;
        this.factor = factor;
        this.min = min;
        this.max = max;
        if (this.factor != 0) {
            this.value = (ThreadLocalRandom.current()
                    .nextInt(this.min, this.max + 1)) / (float)this.factor;
        } else {
            this.value = ThreadLocalRandom.current().nextInt(this.min, this.max + 1);
        }
        this.unit = unit;
    }
    
    public void updateValue() {
        if (this.factor != 0) {
            this.value = (ThreadLocalRandom.current()
                    .nextInt(this.min, this.max + 1)) / (float)this.factor;
        } else {
            this.value = ThreadLocalRandom.current().nextInt(this.min, this.max + 1);
        }
    }
    
    @JsonIgnore
    public String getData() {
        return this.name + "\nFaktor: " + this.factor + 
                "\nRaspon: " + this.min + " do " + this.max + 
                "\nVrijednost: " + this.value + "\nJedinica: " + this.unit + "\n";
    }
    
    public String getName() {
        return this.name;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario.java_vjezba3;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author Mario
 */
public class App {
    
    /**
     * @param args the command line arguments
     * @throws org.eclipse.paho.client.mqttv3.MqttException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws MqttException, IOException {
        
        String meterConfigFilePath = "watermeter.json";

        ObjectMapper mapper = new ObjectMapper();

	// Java object to JSON file
	mapper.writeValue(new File(meterConfigFilePath), new WaterFlowMeter());

	// JSON file to Java object
        WaterFlowMeter waterMeter = mapper.readValue(new File(meterConfigFilePath), WaterFlowMeter.class);
        waterMeter.publish();        
        
    }
}

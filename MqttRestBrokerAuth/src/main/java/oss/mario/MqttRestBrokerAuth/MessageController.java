package oss.mario.MqttRestBrokerAuth;

import java.security.Principal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MessageController {

    private MqttPublisher mqttPublisher;

    public MessageController(MqttPublisher mqttPublisher) {
            this.mqttPublisher = mqttPublisher;
    }

    @GetMapping("/message")
    public String displayMessageForm(Model model, Principal principal) {
        model.addAttribute("message", new Message());
        return "message";
    }

    @PostMapping("/message")
    public String publish(@ModelAttribute Message message, Principal principal) {	
        boolean published = mqttPublisher.publish(message);
        if (!published) {
            return "publish-error";
        }
        return "published";
    }
}

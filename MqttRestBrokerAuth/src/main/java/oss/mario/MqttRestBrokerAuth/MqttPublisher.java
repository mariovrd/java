package oss.mario.MqttRestBrokerAuth;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.springframework.stereotype.Component;

@Component
public class MqttPublisher {

	private MqttAsyncClient client;
	
	public MqttPublisher() {
		try {
			client = new MqttAsyncClient("tcp://192.168.0.14:1883", "12345");
			client.connect();
		} catch (MqttException e) {
			e.printStackTrace();
		} 
	}

	public boolean publish (Message msg)  {
		
		try {
			client.publish(msg.getTopic(), new MqttMessage(msg.getMessage().getBytes()));
			return true;
		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
		
		return false;
	}
}

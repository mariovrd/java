/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario.java_vjezba5;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import javax.swing.SwingUtilities;

/**
 *
 * @author Mario
 */
public class MqttSubscriberApp {

    /**
     * @param args the command line arguments
     * @throws org.eclipse.paho.client.mqttv3.MqttException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws MqttException, IOException {
        
        String configFilePath = "sub.json";

        ObjectMapper mapper = new ObjectMapper();
        
        mapper.disable(MapperFeature.AUTO_DETECT_CREATORS,
            MapperFeature.AUTO_DETECT_FIELDS,
            MapperFeature.AUTO_DETECT_GETTERS,
            MapperFeature.AUTO_DETECT_IS_GETTERS, 
            MapperFeature.AUTO_DETECT_SETTERS);
        
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        
	// Java object to JSON file
	// mapper.writeValue(new File(configFilePath), new MqttSubscriber());        
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() { 
                try {
                    // JSON file to Java object
                    MqttSubscriber subscriber = mapper.readValue(new File(configFilePath), MqttSubscriber.class);
                } catch (IOException ex) {
                    Logger.getLogger(MqttSubscriberApp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
}

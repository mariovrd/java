/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mario.java_vjezba5;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author Mario
 */
public class MqttSubscriber extends JFrame implements MqttCallback {
    
    @JsonProperty
    private String broker;
    @JsonProperty
    private String topic;
    @JsonProperty
    private int windowWidth;
    @JsonProperty
    private int windowHeight;
    
    private MqttClient subscriber;
    
    private JPanel panel;
    
    public MqttSubscriber() throws MqttException {

        this.broker = "tcp://192.168.0.14:1883";
        this.topic = "mjerac";
        this.windowWidth = 600;
        this.windowHeight = 600;
        
        this.subscriber = new MqttClient(this.broker, "client-subscriber", new MemoryPersistence());
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        this.subscriber.setCallback(this);
        this.subscriber.connect(connOpts);
        this.subscriber.subscribe(this.topic + "/#");
        System.out.println("Subscribed");

        this.setWindow();

    }
    
    @JsonCreator
    public MqttSubscriber(@JsonProperty("broker") String broker, 
            @JsonProperty("topic") String topic, @JsonProperty("windowWidth") int windowWidth,
            @JsonProperty("windowHeight") int windowHeight) throws MqttException {

        this.broker = broker;
        this.topic = topic;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        
        this.subscriber = new MqttClient(this.broker, "client-subscriber", new MemoryPersistence());
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        this.subscriber.setCallback(this);
        this.subscriber.connect(connOpts);
        this.subscriber.subscribe(this.topic + "/#");
        System.out.println("Subscribed");

        this.setWindow();
    }

    private void setWindow() {
        // Set layout, dimensions and padding for GUI
        panel = new JPanel( new GridLayout(4, 2, 10, 10));
        Border padding = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        panel.setBorder(padding);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(panel, BorderLayout.CENTER);
        setTitle("Mqtt Subscriber");
        setSize(this.windowWidth, this.windowHeight);
        setVisible(true);
    }
    
    @JsonGetter
    public String getBroker() {
        return broker;
    }

    @JsonSetter
    public void setBroker(String broker) {
        this.broker = broker;
    }

    @JsonGetter
    public String getTopic() {
        return topic;
    }

    @JsonSetter
    public void setTopic(String topic) {
        this.topic = topic;
    }

    @JsonGetter
    public int getWindowWidth() {
        return windowWidth;
    }

    @JsonSetter
    public void setWindowWidth(int windowWidth) {
        this.windowWidth = windowWidth;
    }

    @JsonGetter
    public int getWindowHeight() {
        return windowHeight;
    }

    @JsonSetter
    public void setWindowHeight(int windowHeight) {
        this.windowHeight = windowHeight;
    }
    
    public void displayMessage(String topic, MqttMessage message) {
        
        boolean exists = false;
        
        // Check if topic already exists
        for(Component c : panel.getComponents()) {
            if (c instanceof JLabel && ((JLabel) c).getText().equals(topic)) {
                exists = true;
            }
        }
        // If exists, change values
        if(exists) {
            for(Component c : panel.getComponents()) {
                if (c instanceof JTextArea && c.getName().equals(topic)) {
                    ((JTextArea) c).setText(message.toString());
                }
            }
        }
        else {
            JLabel label = new JLabel(topic);
            JTextArea textArea = new JTextArea(message.toString());
            textArea.setName(topic);
            textArea.setEditable(false);
            Border padding = BorderFactory.createEmptyBorder(10, 10, 10, 10);
            textArea.setBorder(padding);
            this.panel.add(label);
            this.panel.add(textArea);
        }
        
        panel.revalidate();
        panel.repaint();
    }

    @Override
    public void connectionLost(Throwable thrwbl) {
        System.out.println("[ERROR] Lost connection to broker");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        displayMessage(topic, message);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
        
    }


    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oss.mario.mqttrestbroker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mario
 */
@EnableResourceServer
@RestController
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter
{
	@Autowired
	MqttPublisher publisher;
	@PostMapping(path = "/publish")
	public void publish(@RequestBody Message msg) {
            publisher.publish(msg);
            System.out.println("post " + msg.toString());
	}

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http
                .authorizeRequests().antMatchers("/oauth/token", "/oauth/authorize**").permitAll();

            http.requestMatchers().antMatchers("/publish")
                .and().authorizeRequests()
                .antMatchers("/publish").access("hasRole('ADMIN')");
        }   

}
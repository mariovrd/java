package oss.mario.mqttrestbroker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqttRestBrokerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MqttRestBrokerApplication.class, args);
	}

}